package stepDefinition;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by akislinskiy on 06.07.2016.
 */
public class SmokeTest {

    WebDriver driver;

    @Before
    public void testStart(){
     driver=new FirefoxDriver();
     driver.manage().window().maximize();
        System.out.println("Before method");
    }

    @Before("@web")
    public void testTag(){
         System.out.println("Before method with tag");
    }


    @After("@web")
    public void testTag2(){
        System.out.println("After method with tag");


    }


    @After
    public void testShutDown(){
        System.out.println("After method");
        driver.quit();

    }




    @Given("^Open firefox and start application$")
    public void Open_firefox_and_start_application() throws Throwable {
        driver.get("https://www.facebook.com/");
        System.out.println("SmokeTest Open firefox and start application");

    }

    @When("^I enter valid \"(.*?)\" and valid \"(.*?)\"$")
    public void I_enter_valid_username_and_valid_password(String uname, String passw) throws Throwable {
        driver.findElement(By.id("email")).sendKeys(uname);
        driver.findElement(By.id("pass")).sendKeys(passw);
        System.out.println("SmokeTest I enter valid username and valid password");

    }

    @Then("^user should be able to login successfully$")
    public void user_should_be_able_to_login_successfully() throws Throwable {
        driver.findElement(By.id("loginbutton")).click();
        System.out.println("SmokeTest user should be able to login successfully");


    }



    @Then("^I call error$")
    public void i_call_error() throws Throwable {
        driver.findElement(By.id("blahblah")).click();
    }

//   ("^I close browser$")
//    public void i_close_browser() throws Throwable {
//        System.out.println("I close the broser");
//    }



}
