package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by akislinskiy on 06.07.2016.
 */


@RunWith(Cucumber.class)
@CucumberOptions(features = "features", tags = {"@run"}, format = {"pretty", "html:target/cucumber-html-report_pretty"}, glue = {"stepDefinition"}, plugin = {"html:target/cucumber-html-report"})
public class TestRunner {

}
